<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Dog;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Form\DogType;

/**
 * @Route("/api/dog", name="api_dog")
 */
class ApiDogController extends Controller
{
    /**
     * @Route("/", methods="GET")
     */
    public function findAll()
    {
        $dogs = $this->getDoctrine()
        ->getRepository(Dog::class)
        ->findAll();

        $serializer = $this->get('jms_serializer');

        return JsonResponse::fromJsonString(
            $serializer->serialize($dogs, 'json')
        );
    }
     /**
     * @Route("/single/{id}", methods="GET")
     */
    public function find(Dog $dog)
    {
        $serializer = $this->get('jms_serializer');

        return JsonResponse::fromJsonString(
            $serializer->serialize($dog, 'json')
        );
    }
    /**
     * @Route("/user", methods="GET")
     */
    public function findByUser() {
        //On va chercher les chiens du user actuellement connecté
        $dogs = $this->getUser()->getDogs();

        $serializer = $this->get('jms_serializer');

        return JsonResponse::fromJsonString(
            $serializer->serialize($dogs, 'json')
        );
    }
    /**
     * @Route("/", methods="POST")
     */
    public function addDog(Request $request) {
        $manager = $this->getDoctrine()->getManager();
        $form = $this->createForm(DogType::class);
        //On décode le json en array associatif
        $data = json_decode($request->getContent(), true);
        //On submit le formulaire manuellement en lui donnant les données à traiter
        $form->submit($data);
        //On récupère l'entité du formulaire
        $dog = $form->getData();
        //On assigne le user actuellement connecté comme maitre du chien
        $dog->setUser($this->getUser());
        
        $dog->setBase64Image($dog->getPicture());

        $manager->persist($dog);
        $manager->flush();

        $serializer = $this->get('jms_serializer');

        return JsonResponse::fromJsonString(
            $serializer->serialize($dog, 'json'), 201
        );
    }
}
