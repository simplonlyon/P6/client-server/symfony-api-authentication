<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DogRepository")
 */
class Dog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $breed;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $picture;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="dogs")
     */
    private $user;

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBreed(): ?string
    {
        return $this->breed;
    }

    public function setBreed(string $breed): self
    {
        $this->breed = $breed;

        return $this;
    }

    public function getPicture()
    {
        return $this->picture;
    }

    public function setPicture($picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }


    const IMAGES_PATH = 'upload/';
    const MIME_TYPES = ['image/jpeg', 'image/png'];
    public function setBase64Image(String $base64Image)
    {
        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode(',', $base64Image);
        // we could add validation here with ensuring count( $data ) > 1
        $decodedImage = base64_decode($data[1]);
        // open the output file for writing
        $file = fopen('/tmp/tempImage', 'wb');
        fwrite($file, $decodedImage);
        // clean up the file resource
        fclose($file);
        $imageType = mime_content_type('/tmp/tempImage');
        // if mime type is allowed
        if (in_array($imageType, self::MIME_TYPES)){
            // if an image is already attached to the product ...
            
            // generate a unique filename
            $filename = md5(uniqid());
            // get extension e.g png
            preg_match('/.*\/(.*)/', $imageType, $matches);
            $extension = $matches[1];
            // construct file path + name + extension string
            $filePath = self::IMAGES_PATH . $filename . '.' . $extension;
            // move from template to our storage folder
            rename('/tmp/tempImage', $filePath);
            // save picture
            $this->picture = $filePath;
        }
        return $this;
    }
}
